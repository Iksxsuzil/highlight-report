// ==UserScript==
// @name         Highlight Report
// @namespace    highlight-report.user.js
// @version      0.1
// @include      /https://(redacted\.ch|orpheus\.network)/
// ==/UserScript==

document.head.insertAdjacentHTML('beforeend', '<style>.tl_notice { color: orange !important; } .tl_reported { color: red !important; }</style>');
